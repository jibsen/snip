
snip - file cutter
==================

Copyright 1998-2013 Joergen Ibsen

<http://www.ibsensoftware.com/>


Introduction
------------

snip is another bin2h/cut type program, which allows you to convert binary
files to text files that can be included in source code, or simply cut out
part of a file.


Syntax
------

    Syntax:  snip [options] <Input-file> <Output-file> [Start] [End]

    Options: (default output is binary)

      -a : Output as assembler style data
      -c : Output as hex C style data
      -d : Output as compact decimal C style data
      -u : Output LF only instead of CRLF

If no positions are given, snip will process the whole file. If only one
position is given, it will be used as the starting position.

The positions can be in dec, hex, or oct (hex with leading 0x, octal with
leading 0).

To avoid incompatibilities with different languages and compilers, snip does
not add a label to the generated data -- simply add your own where you do the
include.


Example
-------

An example would be something like

    snip -c data.bin data.h

included in a C/C++ file with

    const unsigned char data[] = {
    #include "data.h"
    };
